import java.util.*;

public class Sorting {
	
	/*
	 * time=O(n)
	 * space=O(n)
	 */
	private static array concatenate(array l, int m, array r) {
		array a = new array();
		for(int i=0; i < l.size(); ++i) a.add(l.get(i));
		a.add(m);
		for(int i=0; i < r.size(); ++i) a.add(r.get(i));
		return a;
	}

	/*
	 * time=O(n)
	 * space=O(n)
	 */
	private static array subarray(array a, int s, int e) {
		array t = new array();
		for(int i=s; i <= e; ++i) t.add(a.get(i));
		return t;
	}

	/*
	 * Bubble Sort
	 * time=O(n^2)
	 * space=O(1)
	 */
	public static void bubbleSort(array a) {
		if(a.size()<=1) return;

		for(int i=0; i < a.size(); ++i) {
			for(int j=0; j < a.size()-1; ++j) {
				if(a.get(j) <= a.get(j+1)) continue;
				int tmp = a.get(j);
				a.set(j, a.get(j+1));
				a.set(j+1,tmp);
			}
		} 
	}
	
	/* 
	 * Quick Sort
	 * time=O(n^2)
	 * space=O(log(n))
	 */
	public static array quickSort(array a) {
		if(a.size() <= 1) return a;

		int m = a.get(a.size()/2);
		array l = new array();
		array r = new array();

		for(int i=0; i < a.size(); ++i) {
			if(i == a.size()/2) continue;
			if(a.get(i) <= m) l.add(a.get(i));
			else r.add(a.get(i));
		} return concatenate(quickSort(l), m, quickSort(r));
	}

	/*
	 * Insertion Sort
	 * time=O(n^2)
	 * space=O(1)
	 */
	public static void insertionSort(array a) {
		if(a.size() <= 1) return;
		for(int i=0; i < a.size(); ++i) {
			int j;
			for(j=i; j > 0 && a.get(i) < a.get(j-1); --j);
			int tmp = a.get(i);
			for(int k=i; k > j; --k) a.set(k, a.get(k-1));
			a.set(j, tmp);
		}
	}

	/*
	 * Counting Sort
	 * time=O(n+k)
	 * space(k)
	 */
	public static void countingSort(array a) {
		if(a.size() <= 1) return;
		array t = new array(); int k=0;

		for(int i=0; i < a.size(); ++i) if(a.get(i) > k) k = a.get(i);
		for(int i=0; i <= k; ++i, t.add(0));
		for(int i=0; i < a.size(); ++i) t.set(a.get(i), t.get(a.get(i))+1);
		for(int i=0, p=0; i < k+1 && p < a.size(); ++i) {
			for(;t.get(i)!=0;t.set(i, t.get(i)-1)) a.set(p++, i);
		}
	}

	/*
	 * Merge Sort
	 * time=O(nlog(n))
	 * space=O(1)
	 */
	public static array mergeSort(array a) {
		if(a.size() <= 1) return a;

		int m = a.size()/2;
		array left = mergeSort(subarray(a, 0, m-1));
		array right = mergeSort(subarray(a, m, a.size()-1));
		array res = new array();
		for(int i=0; i < a.size(); ++i) res.add(0);

		for(int d=0, l=0, r=0; d < a.size(); ++d) {
			if(l == left.size()) res.set(d, right.get(r++));
			else if(r == right.size()) res.set(d, left.get(l++));
			else if(left.get(l) < right.get(r)) res.set(d, left.get(l++));
			else res.set(d, right.get(r++));
		} return res;
	}

	/*
	 * Heap Sort
	 * time=O(nlog(n))
	 * space=O(n)
	 */
	public static void heapSort(array a) {
		if(a.size() <= 1) return;
		PriorityQueue<Integer> heap = new PriorityQueue<Integer>(a.size());
		for(int i=0; i < a.size(); ++i) heap.offer(a.get(i));
		for(int i=0; i < a.size(); ++i) {
			a.set(i, heap.poll());
		} 
	}

	/*
	 * Binary Search
	 * time=O(log(n))
	 * space=O(1)
	 */
	public static int binarySearch(array a, int key) {
		if(a.size()==0) return -1;

		int mid = a.size()/2;
		int min = 0;
		int max = a.size()-1;

		for(;a.get(mid)!=key && min < max; mid=(min+max)/2) {
			if(a.get(mid) > key) max=mid-1;
			else if(a.get(mid) < key) min=mid+1;
		} if(a.get(mid)==key) return mid;
		else return -1;
	}

	/*
	 * Fibonacci Sequence
	 * time=O(2^n)
	 * space=O(1)
	 */
	public static int fibonacci(int n) {
		return (n<=1) ? n : (fibonacci(n-1) + fibonacci(n-2));
	}

	// main for testing
	public static void main(String[] args) {
		Random r = new Random();
		array a = new array();

		System.out.print("input: ");
		for(int i=0; i < 8; ++i) {
			a.add(r.nextInt(10) + 1);
			System.out.print(a.get(i) + " ");
		} System.out.println();

		array a2 = new array();
		for(int i=0; i < 8; ++i) a2.add(a.get(i));
		
		System.out.print("bubble sort: ");
		Sorting.bubbleSort(a2);
		for(int i=0; i < a2.size(); ++i) System.out.print(a2.get(i) + " ");
		System.out.println(); a2.clear();

	    System.out.print("quick sort: ");
		a2 = Sorting.quickSort(a);
		for(int i=0; i < a2.size(); ++i) System.out.print(a2.get(i) + " ");
		System.out.println(); a2.clear();

		System.out.print("insertion sort: ");
		for(int i=0; i < 8; ++i) a2.add(a.get(i));
		Sorting.insertionSort(a2);
		for(int i=0; i < a2.size(); ++i) System.out.print(a2.get(i) + " ");
		System.out.println(); a2.clear();

		System.out.print("counting sort: ");
		for(int i=0; i < 8; ++i) a2.add(a.get(i));
		Sorting.countingSort(a2);
		for(int i=0; i < a2.size(); ++i) System.out.print(a2.get(i) + " ");
		System.out.println(); a2.clear();

		System.out.print("merge sort: ");
		a2 = Sorting.mergeSort(a);
		for(int i=0; i < a2.size(); ++i) System.out.print(a2.get(i) + " ");
		System.out.println(); a2.clear();

		System.out.print("heap sort: ");
		for(int i=0; i < 8; ++i) a2.add(a.get(i));
		Sorting.heapSort(a2);
		for(int i=0; i < a2.size(); ++i) System.out.print(a2.get(i) + " ");
		System.out.println();

		System.out.println(); 

		// test binary search
		System.out.print("binary search: 1\t");
		System.out.println("index: "+binarySearch(a2, 1));
		System.out.print("binary search: 7\t");
		System.out.println("index: "+binarySearch(a2, 7));
		System.out.print("binary search: 3\t");
		System.out.println("index: "+binarySearch(a2, 3));
		System.out.print("binary search: 5\t");
		System.out.println("index: "+binarySearch(a2, 5));
		System.out.print("binary search: 10\t");
		System.out.println("index: "+binarySearch(a2, 10));
		System.out.print("binary search: 6\t");
		System.out.println("index: "+binarySearch(a2, 6));
		System.out.println();
		
		// test fibonacci
		System.out.println("fibonacci(5) = "+fibonacci(5));
		System.out.println("fibonacci(10) = "+fibonacci(10));
	}
}

// dynamically sized int array
class array {

	// class privates
	private int size, max;
	private int[] a;

	// constructor
	public array() {
		max=10;
		a = new int[max];
	}
	
	// public methods
	public int size() { return size; }
	public void add(int i) {
		if(size==max) resize();
		a[size++] = i;
	}
	public int get(int i) { return a[i]; }
	public void set(int i, int n) { a[i] = n; }
	public void clear() {
		for(int i=0; i < size; ++i) {
			a[i] = 0;
		} size = 0;
	}

	// private method
	private void resize() {
		if(size < max) return;
		max*=2;
		int[] tmp = new int[max];
		for(int i=0; i < size; ++i ) tmp[i]=a[i];
		a = tmp;
	}
}
