#ifndef __SORTING_HPP__
#define __SORTING_HPP__

#include <iostream>
#include <math.h>
#include <vector>
#include <queue>

class sorting {
	// TODO: figure out how to fit heap sort in here
	std::vector<int> concatenate(std::vector<int> l, int m, std::vector<int> r);
	std::vector<int> subarray(std::vector<int> a, int s, int e);
public:
	std::vector<int> quick_sort(std::vector<int> array);
	std::vector<int> bubble_sort(std::vector<int> array);
	std::vector<int> insertion_sort(std::vector<int> array);
	std::vector<int> counting_sort(std::vector<int> array);
	std::vector<int> merge_sort(std::vector<int> array);
	std::vector<int> heap_sort(std::vector<int> array);
	std::vector<int> hybrid_sort(std::vector<int> array);
	// search algorithm
	int binary_search(std::vector<int> array, int key);
	// fibonacci recursion
	int fibonacci(int n);
};
#endif
