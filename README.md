# Algorithms and Data Structures

## Introduction
These are just basic sorting algorithms and practice interview questions solved
in C++ and Java based on Big-O notation (worst case scenario).

Side note, this is how to run it through linux command line:
```
$ make
$ ./sort
$ java Sorting
$ ./parse
$ ./number
$ cd practice
$ java ParseStrings
```

## [Bubble Sort](https://www.topcoder.com/community/data-science/data-science-tutorials/sorting/)
The algorithm works by comparing each item in the list with the item next to it, and
swapping them if required. In other words, the largest element has bubbled to the top
of the array. The algorithm repeats this process until it makes a pass all the way
through the list without swapping any items.

*Time Complexity*: O(n^2
)

*Space Complexity*: O(1)


## [Quick Sort](https://www.topcoder.com/community/data-science/data-science-tutorials/sorting/)
A quick sort, as the name implies, is intended to be an efficient sorting algorithm.
The theory behind it is to sort a list in a way very similar to how a human might do
it. First, divide the data into two groups of “high” values and “low” values. Then,
recursively process the two halves. Finally, reassemble the now sorted list.

*Time Complexity*: O(n^2
)

*Space Complexity*: O(log(n))


## [Insertion Sort](https://www.topcoder.com/community/data-science/data-science-tutorials/sorting/)
To sort unordered list of elements, we remove its entries one at a time and then
insert each of them into a sorted part (initially empty).

*Time Complexity*: O(n^2
)

*Space Complexity*: O(1)


## [Counting Sort](https://en.wikipedia.org/wiki/Counting_sort)
Take the domain of possible variables (0 _<_ x _<_ k), and create an array of
size k+1 to include zero. Then, loop through the original array and count each
type of integer.  Store the count in the index of the array of size k+1 of the
integer.
So, an array of [1,2,1,3] would translate to another array of [0,2,1,1].  There are
zero 0s, two 1s, one 2, and one 3.

*Time Complexity*: O(n+k)

*Space Complexity*: O(k)


## [Merge Sort](https://www.topcoder.com/community/data-science/data-science-tutorials/sorting/)
Merge-sort is based on the divide-and-conquer paradigm. It involves the following
three steps:
1. Divide the array into two (or more) subarrays
2. Sort each subarray (Conquer)
3. Merge them into one (in a smart way!)

*Time Complexity*: O(nlog(n))

*Space Complexity*: O(n)


## [Heap Sort](https://www.topcoder.com/community/data-science/data-science-tutorials/sorting/)
In a heap sort, we create a heap data structure. A heap is a data structure that
stores data in a tree such that the smallest (or largest) element is always the root
node. To perform a heap sort, all data from a list is inserted into a heap, and then
the root element is repeatedly removed and stored back into the list.

*Time Complexity*: O(nlog(n))

*Space Complexity*: O(n)

## Hybrid Sort
This is a sorting algorithm that combines counting sort and merge sort to create
an optimal storing algorithm. It runs through an array of size `n` once to find
the largest entry, `k`. If the entry, `k`, is greater than `n * log(n)`, then a
merge sort is performed on the array. Otherwise, counting sort is performed.


*Time Complexity*: O(nlog(n))

*Space Complexity*: O(k) - if k < nlog(n), O(n) - if k > nlog(n)

# Other Algorithms

## [Binary Search](http://quiz.geeksforgeeks.org/binary-search/)
Search a sorted array by repeatedly dividing the search interval in half. Begin with
an interval covering the whole array. If the value of the search key is less than the
item in the middle of the interval, narrow the interval to the lower half. Otherwise
narrow it to the upper half. Repeatedly check until the value is found or the
interval is empty.

*Time Complexity*: O(log(n))

*Space Complexity*: O(1)

## [Fibonacci Sequence](https://www.mathsisfun.com/numbers/fibonacci-sequence.html)
The Fibonacci Sequence is the series of numbers, where the next number is found by
adding up the two numbers before it: 0, 1, 1, 2, 3, 5, 8...

*Time Complexity*: O(2^n
)

*Space Complexity*: O(1)

# Practice Question

## Linked List Addition

You are given two linked list that represent two numbers, but they are in reverse
order. Write a function that takes these two linked lists and adds them together to
create a new linked list, then returns its head.

Example:
```
number to represent = 365
number to represent = 42

LList one: 5->6->3
LList two: 2->4

LList res = add(one, two);

res: 7->0->4
```

## Counting Characters

You are given a string. Write a function that finds the most reoccuring character
within the given string and returns it.

Example:
```
String given = "aaaabc"

char c = countChar(given);

c: a
```

## Is Balanced?

You are given a string that contains a set of characters. Write a function to check
if the given string has a balanced number of brackets (parentheses, square brackets,
curly braces, less than/greater than brackets) that returns true if there are a
correct number of brackets and false otherwise.

Example:
```
String nope = "function(a[]))"
String yup = "function(a[])"

bool nope_balanced = isBalanced(nope);
bool yup_balanced = isBalanced(yup);

nope_balanced: false
yup_balanced: true
```

## Isomorphic Strings

Given two strings s and t, determine if they are isomorphic.

Two strings are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while
preserving the order of characters. No two characters may map to the same character
but a character may map to itself.

Example:
```
Given "egg", "add", return true.

Given "foo", "bar", return false.

Given "paper", "title", return true.
```

## Anagrams

Given two strings a and b, determine the number of total characters that needs
to be removed from each so that a and b are anagrams.

Example:
```
string a = "cde"
string b = "abc"

int total = anagram(a, b)

total: 4

// the anagram would be "c" by removing "d" and "e" from a, and "a" and "b" from b
```

## FalloutHacker

Given a string, `s`, and an integer, `c`, run through a list of strings, `l`,
that will return all strings that have the same number of letters in common
in the same location, `c`, as the given string, `s`.

Example:
```
string s = "cat"
int c = 1
string[] l = ["dog", "sat", "dot", "mat", "can", "pan"]

string[] result = fallouthacker(s, c, l)

result: ["dot", "pan"]

// "dot" matches "cat" once at 't', and "pan" matches "cat" once at 'a'
```
