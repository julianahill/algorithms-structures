#include "sorting.hpp"

/*
 * time=O(n)
 * space=O(n)
 */
std::vector<int> sorting::concatenate(std::vector<int> l,int m, std::vector<int> r) {
	std::vector<int> v;
	for(int i=0; i<l.size(); ++i) v.push_back(l[i]);
	v.push_back(m);
	for(int i=0; i<r.size(); ++i) v.push_back(r[i]);
	return v;
}

/*
 * time=O(n)
 * space=O(n)
 */
std::vector<int> sorting::subarray(std::vector<int> a, int s, int e) {
	std::vector<int> v;
	for(int i=s; i<=e; ++i) v.push_back(a[i]);
	return v;
}

/*
 * Quick Sort (recursive)
 * time=O(n^2)
 * space=O(log(n))
 */
std::vector<int> sorting::quick_sort(std::vector<int> array) {
	if(array.size() <= 1) return array;

	int middle = array[array.size()/2];
	std::vector<int> left;
	std::vector<int> right;

	for(int i=0; i < array.size(); ++i) {
		if(i != array.size()/2) {
			if(array[i] <= middle)
				left.push_back(array[i]);
			else
				right.push_back(array[i]);
		}
	} return concatenate(quick_sort(left), middle, quick_sort(right));
}

/*
 * Bubble Sort
 * time=O(n^2)
 * space=O(1)
 */
std::vector<int> sorting::bubble_sort(std::vector<int> array) {
	if(array.size() <= 1) return array;
	for(int i=0; i < array.size(); ++i) {
		for(int j=0; j < array.size()-1; ++j) {
			if(array[j] <= array[j+1]) continue;
			int tmp = array[j];
			array[j] = array[j+1];
			array[j+1] = tmp;
		}
	} return array;
}

/*
 * Insertion Sort
 * time=O(n^2)
 * space=O(1)
 */
std::vector<int> sorting::insertion_sort(std::vector<int> array) {
	if(array.size() <=1) return array;
	for(int i=0; i < array.size(); ++i) {
		int j;
		for(j=i; j > 0 && array[i] < array[j-1]; --j);
		int tmp = array[i];
		for(int k=i; k > j; --k) array[k] = array[k-1];
		array[j] = tmp;
	} return array;
}

/*
 * Merge Sort (recursive)
 * time=O(nlog(n))
 * space=O(n)
 */
std::vector<int> sorting::merge_sort(std::vector<int> array) {
	if(array.size() <= 1) return array;

	int middle = (array.size()/2);

	std::vector<int> left = merge_sort(subarray(array, 0, middle-1));
	std::vector<int> right = merge_sort(subarray(array, middle, array.size()-1));
	std::vector<int> result(array.size());

	for(int d=0, l=0, r=0; d < array.size(); ++d) {
		if(l == left.size()) result[d] = right[r++];
		else if(r == right.size()) result[d] = left[l++];
		else if( left[l] < right[r] ) result[d] = left[l++];
		else result[d] = right[r++];
	} return result;
}

/*
 * Counting Sort
 * time=O(n+k)
 * space=O(k)
 * assuming domain of values starts at 0 and ends at k
 */
std::vector<int> sorting::counting_sort(std::vector<int> array) {
	if(array.size()<=1) return array;
	int k=0;

	for(int i=0; i < array.size(); ++i) if(array[i] > k) k=array[i];
	std::vector<int> count(k);
	for(int i=0; i < array.size(); ++i) ++count[array[i]];
	for(int i=0, p=0;i < k+1 && p < array.size(); ++i) {
		for(;count[i];count[i]--) array[p++]=i;
	} return array;
}

/*
 * Heap Sort
 * time=O(nlog(n))
 * space=O(n)
 */
std::vector<int> sorting::heap_sort(std::vector<int> array) {
	if(array.size()<=1) return array;
	std::priority_queue<int> heap;
	for(int i=0; i < array.size(); ++i) heap.push(array[i]);
	for(int i=array.size()-1; i>=0; --i) {
		array[i] = heap.top();
		heap.pop();
	} return array;
}

/*
 * Hybrid Sort
 * time:
 *  worst=O(nlog(n))
 *  best=O(n+k)
 * space:
 *  O(k) // if k < nlog(n)
 *  O(n) // if k > nlog(n)
 */
std::vector<int> sorting::hybrid_sort(std::vector<int> array) {
	if(array.size()<=1) return array;
	int k=0, n=array.size();

	for(int i=0; i < n; ++i) if(array[i] > k) k=array[i];

	if (k > (n * log(n))) return merge_sort(array);
	else return counting_sort(array);
}

/*
 * Binary Search
 * time=O(log(n))
 * space=O(1)
 */
int sorting::binary_search(std::vector<int> array, int key) {
	if(!array.size()) return -1;

	int mid = array.size()/2;
	int min = 0;
	int max = array.size()-1;

	for(;array[mid]!=key && min < max; mid=(min+max)/2) {
		if(array[mid] > key) max=mid-1;
		else if(array[mid] < key) min=mid+1;
	} if(array[mid]==key) return mid;
	else return -1;
}

/*
 * Fibonacci Sequence
 * time=O(2^n)
 * space=O(1)
 */
int sorting::fibonacci(int n) {
    return (n<=1) ? n : (fibonacci(n-1) + fibonacci(n-2));
}

// main for testing
int main() {
	sorting s;
	std::vector<int> fun = {5,5,2,2,3,2,1,4,7};
	std::cout<<"input: ";
	for(int i=0;i < fun.size(); ++i) std::cout<<fun[i]<<' ';
	std::cout<<std::endl;

	// counting sort
	std::cout<<"counting sort: ";
	std::vector<int> res = s.counting_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl; res.clear();

	// bubble sort
	std::cout<<"bubble sort: ";
	res = s.bubble_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl; res.clear();

	// merge sort
	std::cout<<"merge sort: ";
	res = s.merge_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl; res.clear();

	// quick sort
	std::cout<<"quick sort: ";
	res = s.quick_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl; res.clear();

	// insertion sort
	std::cout<<"insertion sort: ";
	res = s.insertion_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl; res.clear();

	// heap sort
	std::cout<<"heap sort: ";
	res = s.heap_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl;

	// hybrid sort
	std::cout<<"hybrid sort: ";
	res = s.heap_sort(fun);

	for(int i=0;i < res.size(); ++i) std::cout<<res[i]<<' ';
	std::cout<<std::endl;

	// test binary search
	std::cout<<std::endl<<"binary search: 1\t";
	std::cout<<"index: "<<s.binary_search(res, 1)<<std::endl;
	std::cout<<"binary search: 7\t";
	std::cout<<"index: "<<s.binary_search(res, 7)<<std::endl;
	std::cout<<"binary search: 3\t";
	std::cout<<"index: "<<s.binary_search(res, 3)<<std::endl;
	std::cout<<"binary search: 5\t";
	std::cout<<"index: "<<s.binary_search(res, 5)<<std::endl;
	std::cout<<"binary search: 10\t";
	std::cout<<"index: "<<s.binary_search(res, 10)<<std::endl;
	std::cout<<"binary search: 6\t";
	std::cout<<"index: "<<s.binary_search(res, 6)<<std::endl;

	// test fibonacci
	std::cout<<std::endl<<"fibonacci(5) = "<<s.fibonacci(5)<<std::endl;
	std::cout<<"fibonacci(10) = "<<s.fibonacci(10)<<std::endl;

	return 0;
}
