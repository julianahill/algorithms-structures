CC = g++ -g -std=c++14

all: sort Sorting parsing Parse number Number FalloutHacker

parsing: practice/parsing_strings.cpp
	$(CC) -o parse practice/parsing_strings.cpp

Parse: practice/ParseStrings.java
	javac practice/ParseStrings.java

sort: sorting.cpp sorting.hpp
	$(CC) -o sort sorting.cpp

Sorting:
	javac Sorting.java

number: practice/number_challenges.cpp
	$(CC) -o number practice/number_challenges.cpp

Number: practice/NumberChallenges.java
	javac practice/NumberChallenges.java

FalloutHacker: practice/FalloutHacker.java
	javac practice/FalloutHacker.java

clean:
	rm -f sort parse number practice/*.class *.class *~ *# practice/*~ practice/*#
