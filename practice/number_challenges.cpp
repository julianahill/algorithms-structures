#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

/*
 * While Queen Elizabeth has a limited number of types of cake, she has an unlimited
 * supply of each type.
 * Each type of cake has a weight and a value, stored in a tuple with two indices:
 *
 * An integer representing the weight of the cake in kilograms
 * An integer representing the monetary value of the cake in British pounds
 * For example:
 *
 * weighs 7 kilograms and has a value of 160 pounds: (7, 160)
 * weighs 3 kilograms and has a value of 90 pounds: (3, 90)
 *
 * You brought a duffel bag that can hold limited weight, and you want to make off 
 * with the most valuable haul possible.
 *
 * Write a function max_duffel_bag_value() that takes a list of cake type tuples and
 * a weight capacity, and returns the maximum monetary value the duffel bag can hold.
 *
 * For example:
 * 
 * cake_tuples = [(7, 160), (3, 90), (2, 15)]
 * capacity    = 20
 *
 * max_duffel_bag_value(cake_tuples, capacity)
 * returns 555 (6 of the middle type of cake and 1 of the last type of cake)
 *
 * Weights and values may be any non-negative integer. Yes, it's weird to think about
 * cakes that weigh nothing or duffel bags that can't hold anything. But we're not 
 * just super mastermind criminals—we're also meticulous about keeping our algorithms
 * flexible and comprehensive.
 */
int max_duffel_bag_value(vector<pair<int, int>> list, int cap) {
   if(!cap) return 0;
   vector<int> max_vals(cap+1);
	
   for(int cur = 0; cur <= cap; ++cur) {
      int cur_max = 0;

      for(auto const & cake : list) {
	 if(cake.first == 0 && cake.second != 0) {
	    throw range_error("Max value is infinity!");
	 }

	 if(cake.first <= cur) {
	    int max_val = cake.second + max_vals[cur - cake.first];
	    cur_max = max(max_val, cur_max);
	 }
      } max_vals[cur] = cur_max;

   } return max_vals[cap];
}

/*
 * Writing programming interview questions hasn't made me rich. 
 * Maybe trading Apple stocks will. Suppose we could access yesterday's stock 
 * prices as a vector, where:
 *
 * The values are the price in dollars of Apple stock.
 * A higher index indicates a later time.
 *
 * Write an efficient function that takes stockPricesYesterday and returns the 
 * best profit I could have made from 1 purchase and 1 sale of 1 Apple stock yesterday.
 *
 * For example:
 * 
 * vector<int> stockPricesYesterday{10, 7, 5, 8, 11, 9};
 * 
 * getMaxProfit(stockPricesYesterday);
 * returns 6 (buying for $5 and selling for $11)
 *
 * No "shorting"—you must buy before you sell. You may not buy and sell 
 * in the same time step (at least 1 minute must pass).
 */
int getMaxProfit(vector<int> yesterday) {
   int biggest = 0;
   int min = yesterday[0]; 

   for(int i=0,cur=yesterday[i]; i < yesterday.size(); ++i, cur=yesterday[i]) {
      if(cur-min > biggest) biggest = cur-min;
      if(cur < min) min=cur;
   } return biggest;
}

int main() {
	// test max_duffel_bag_value
	vector<pair<int,int>> cakes;
	cakes.push_back(pair<int,int>(7, 160));
	cakes.push_back(pair<int,int>(3, 90));
	cakes.push_back(pair<int,int>(2, 15));

	int res = max_duffel_bag_value(cakes, 20);
	cout<<"cake tuples: [ ";
	for(auto const & v : cakes) cout<<"("<<v.first<<", "<<v.second<<") ";
	cout<<"]"<<endl;

	cout<<"capacity: 20"<<endl;
	cout<<"result: "<<res<<endl<<endl;

	// test getMaxProfit
	vector<int> stockPrice{10, 7, 5, 8, 11, 9};

	cout<<"prices: { ";
	for(auto const & a: stockPrice) cout<<a<<" ";
	cout<<"}"<<endl;

	cout<<"result: "<<getMaxProfit(stockPrice)<<endl;
	return 0;
}

/* 
 * A building has 100 floors. One of the floors is the highest floor an egg can be 
 * dropped from without breaking. If an egg is dropped from above that floor, it will
 * break. If it is dropped from that floor or below, it will be completely undamaged
 * and you can drop the egg again.
 * 
 * Given two eggs, find the highest floor an egg can be dropped from without 
 * breaking, with as few drops as possible.
 */
