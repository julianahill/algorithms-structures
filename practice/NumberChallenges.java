import java.util.*;
import java.lang.*;

public class NumberChallenges {

  // check if a number is prime
  public static boolean isPrime(int n) {
    n = Math.abs(n);
  	if (n <= 1) return false;
  	else if (n <= 3) return true;
    else if (n == 5) return true;
    else if (n == 7) return true;
  	else if ( n % 2 == 0 ||
              n % 3 == 0 ||
              n % 5 == 0 ||
              n % 7 == 0 ) return false;
  	else return true;
  }

  // add two linked lists that contain a number in reverse order
  // return the head of the resulting linked list in resversed order
  public static LLNode add(LLNode one, LLNode two) {
  	LLNode result = new LLNode();
  	LLNode next = result;

  	int carry=0;

    while(one!=null || two!=null) {
	    int o=0, t=0;

	    if(one!=null) o=one.data;
	    if(two!=null) t=two.data;

	    int total = carry + o + t;

	    if(total >= 10) next.data = total-10;
	    else next.data = total;

	    carry = total/10;

	    if((one!=null && one.next!=null)
	       || (two!=null && two.next!=null)) {
		         next.next = new LLNode();
		         next = next.next;
	    }

  	  if(one!=null) one=one.next;
  	  if(two!=null) two=two.next;
    } return result;
  }

  public static void main(String[] args) {
  	String result = new String();

  	LLNode one = new LLNode();
  	LLNode next1 = one;

  	one.data = 5;
  	next1.next = new LLNode();
  	next1 = next1.next;

  	next1.data = 6;
  	next1.next = new LLNode();
  	next1 = next1.next;

  	next1.data = 3;

  	LLNode two = new LLNode();
  	LLNode next2 = two;

  	two.data = 2;
  	next2.next = new LLNode();
  	next2 = next2.next;

  	next2.data = 4;

  	LLNode res = add(one, two);
  	for(;res != null; res=res.next) result+=res.data;
  	System.out.println("input: 365, 42");
  	System.out.print("output: ");
  	for(int i=result.length()-1; i > -1; --i)
  	    System.out.print(result.charAt(i));
  	System.out.println();

  	if(isPrime(5)) System.out.println("5 is Prime: true");
  	else System.out.println("5 is Prime: false");

  	if(isPrime(10)) System.out.println("10 is Prime: true");
  	else System.out.println("10 is Prime: false");

  	if(isPrime(50)) System.out.println("50 is Prime: true");
  	else System.out.println("50 is Prime: false");

  	if(isPrime(13)) System.out.println("13 is Prime: true");
  	else System.out.println("13 is Prime: false");
  }
}

class LLNode {
  int data;
  LLNode next;

  LLNode() {
    next = null;
  }
}
