import java.util.*;
import java.lang.*;
import java.io.*;

public class FalloutHacker {
  private ArrayList<String> ps;
  private String s;
  private int c;

  FalloutHacker() {
    ps = new ArrayList<String>();
    s = null;
  }

  public Boolean add(String s) {
    return ps.add(s.toUpperCase().trim());
  }

  public void guessed(String s, Integer i) {
    this.s = new String();
    this.s += s.toUpperCase().trim();
    this.c = i;
  }

  public static int lettersInCommon(String o, String t) {
    if (o.length() != t.length()) return -1;

    int count = 0;
    for (int i = 0; i < o.length(); ++i) {
      if (o.charAt(i) == t.charAt(i)) count++;
    } return count;
  }

  public ArrayList<String> possibleSolutions() {
    System.out.println(s + ":" + c);
    if (s.length() == 0) return null;

    for (int i = 0; i < ps.size(); ++i) {
      if (lettersInCommon(s, ps.get(i)) != c) ps.remove(i--);
    }

    return ps;
  }

  public String generateFirstGuess() {
    if (ps.size() == 0) return "N/A";

    HashMap<String, Integer> g = new HashMap<String, Integer>();

    for (int i = 0; i < ps.size(); ++i) {
      String f = ps.get(i);

      for (int j = 0; j < ps.size(); ++j) {
        if (i == j) continue;

        if (lettersInCommon(f, ps.get(j)) == 1) {
          if(!g.containsKey(f)) g.put(f, 1);
          else g.put(f, g.get(f)+1);
        }
      }
    }

    int biggest = 0;
    String guess = "N/A";
    for(Map.Entry<String, Integer> e : g.entrySet()) {
      if(e.getValue() > biggest) {
        biggest = e.getValue();
        guess = e.getKey();
      }
    }

    return guess;
  }

  public static void main (String[] args) {
    FalloutHacker fh = new FalloutHacker();

    Scanner s = new Scanner(System.in);
    System.out.println("Enter in a word, then press enter.");
    System.out.println("Type \"DONE\" when you're done entering in words.");

    String input = s.next();
    while (input.compareToIgnoreCase("done") != 0) {
      fh.add(input.toUpperCase());
      input = s.next();
    }

    System.out.println();
    System.out.println("If you want me to guess a word, type in \"GUESS\",");
    System.out.println("otherwise enter the word you guessed, press enter, ");
    System.out.println("the number letters that are correct...press enter.");
    System.out.println();
    System.out.println("If you'd like to exit, type \"DONE\", and press enter.");

    Integer common = 0;
    input = s.next();
    while (input.compareToIgnoreCase("done") != 0) {
      if (input.compareToIgnoreCase("guess") == 0) {
        System.out.println("Computed guess: " + fh.generateFirstGuess());
        System.out.println();
      } else {
        try {
          common = s.nextInt();
          System.out.println();
          fh.guessed(input.toUpperCase(), common);
        } catch (Exception e) {
          e.printStackTrace(System.out);
        }

        ArrayList<String> p = fh.possibleSolutions();
        if (p == null) continue;
        System.out.println("Possible solutions: ");
        for (int i = 0; i < p.size(); ++i) {
          System.out.println(p.get(i));
        }
      } input = s.next();
    }
  }
}
