#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <stack>

using namespace std;

// counts letters and returns the letter with greatest value 
vector<char> count_letters(string s) {
   if(!s.size()) return vector<char>();
	
   vector<char> highest;
   unordered_map<char, int> chars;
	
   for(char c : s) chars[c]++;

   int biggest = 0;
   for(auto const & a : chars) {
      if(a.second > biggest) biggest = a.second;
   } for(auto const & a : chars) {
      if(a.second == biggest) highest.push_back(a.first);
   } return highest;
}

/* 
 * checks to see if a given string has balanced brackets, braces and/or parentheses
 * using a stack
 */
bool is_balanced(string s) {
   if(!s.size()) return true;
   stack<char> st;
   unordered_map<char,char> brackets;

   brackets[')']='(';
   brackets[']']='[';
   brackets['}']='{';
   brackets['>']='<';
	
   for(char c: s) {
      if(c=='(' || c=='[' || c=='{' || c=='<') st.push(c);
      else if(brackets.find(c) != brackets.end()) {
	 if(!st.empty() && brackets.find(c)->second == st.top())
	    st.pop();
	 else return false;
      } 
   } if(st.empty()) return true;
   else return false;
}

/*
 * checks if the two strings are isomorphic
 */
bool are_isomorphic(string s, string t) {
   if(s.size()!=t.size()) return false;
   unordered_map<char,char> s_t;
   unordered_map<char,char> t_s;
   for(int i=0; i<t.size(); ++i) {
      if(s_t.find(s[i]) == s_t.end()
	 && t_s.find(t[i]) == t_s.end()) {
	 s_t[s[i]]=t[i];
	 t_s[t[i]]=s[i];
      } else if((s_t.find(s[i]) != s_t.end() &&
		 s_t.find(s[i])->second != t[i]) ||
		(t_s.find(t[i]) != t_s.end() &&
		 t_s.find(t[i])->second != s[i]))
	 return false;
   } return true;
}

/*
 * returns number of letters to removed to create anagrams
 */
int number_to_remove(string a, string b) {
   if(!a.size() || !b.size()) return b.size() + a.size();
    
   unordered_map<char, int> A;
   unordered_map<char, int> B;
   unordered_map<char, int> l;
    
   for(char c : a) {
      if(l.find(c)==l.end()) l[c]++;
      A[c]++;
   } for(char c : b) {
      if(l.find(c)==l.end()) l[c]++;
      B[c]++;
   }
    
   int total = 0;
   for(auto const & e : l) {
      char c = e.first;
      if(A.find(c) != A.end() && B.find(c) != B.end()) {
	 total+=abs(A.find(c)->second - B.find(c)->second);
      } else {
	 if(A.find(c)!= A.end()) total+=A.find(c)->second;
	 if(B.find(c)!= B.end()) total+=B.find(c)->second;
      }
   } return total;
}

int main() {
   // test count_letters
   string a = "aaaabc";
   string b = "abbbbbbc";
   string all = "abcd";
   string name = "ana";
   string empty = "";

   cout<<"input: "<<a<<endl;
   vector<char> res = count_letters(a);
   cout<<"output: ";
   for(int i=res.size()-1; i > -1; --i) cout<<res[i]<<" ";
   cout<<endl;

   cout<<"input: "<<b<<endl;
   res = count_letters(b);
   cout<<"output: ";
   for(int i=res.size()-1; i > -1; --i) cout<<res[i]<<" ";
   cout<<endl;

   cout<<"input: "<<all<<endl;
   res = count_letters(all);
   cout<<"output: ";
   for(int i=res.size()-1; i > -1; --i) cout<<res[i]<<" ";
   cout<<endl;

   cout<<"input: "<<empty<<endl;
   res = count_letters(empty);
   cout<<"output: ";
   for(int i=res.size()-1; i > -1; --i) cout<<res[i]<<" ";
   cout<<endl;

   cout<<"input: "<<name<<endl;
   res = count_letters(name);
   cout<<"output: ";
   for(int i=res.size()-1; i > -1; --i) cout<<res[i]<<" ";
   cout<<endl;

   // test balance stack
   string test = "(<)<<>]]";
   string test2 = "<{[(())]}>";
   string test3 = "[ ] [ ] [ ]";
   string test4 = "(((((";
   string test5 = " )";

   cout<<endl<<"input: "<<test<<endl;
   if(is_balanced(test)) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: "<<test2<<endl;
   if(is_balanced(test2)) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: "<<test3<<endl;
   if(is_balanced(test3)) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;
	
   cout<<endl<<"input: "<<test4<<endl;
   if(is_balanced(test4)) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;
	
   cout<<endl<<"input: "<<test5<<endl;
   if(is_balanced(test5)) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   // test isomorphism
   cout<<endl<<"input: egg, add"<<endl;
   if(are_isomorphic(string("egg"),string("add"))) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: foo, bar"<<endl;
   if(are_isomorphic(string("foo"),string("bar"))) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: paper, title"<<endl;
   if(are_isomorphic(string("paper"),string("title")))
      cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: aa, ac"<<endl;
   if(are_isomorphic(string("aa"),string("ac"))) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;
	
   cout<<endl<<"input: ac, aa"<<endl;
   if(are_isomorphic(string("ac"),string("aa"))) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: ca, ab"<<endl;
   if(are_isomorphic(string("ca"),string("ab"))) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;

   cout<<endl<<"input: ab, ca"<<endl;
   if(are_isomorphic(string("ab"),string("ca"))) cout<<"output: true"<<endl;
   else cout<<"output: false"<<endl;
}
