import java.util.*;
import java.lang.*;

public class ParseStrings {
   // counts letters and returns the letter with greatest value
   public static String countLetters(String s) {
      if(s.length()<=1) return s;

      String highest = new String();
      HashMap<Character, Integer> count = new HashMap<Character, Integer>();

      for(int i=0; i < s.length(); ++i) {
    	 if(!count.containsKey(s.charAt(i))) count.put(s.charAt(i), 1);
    	 else count.put(s.charAt(i), count.get(s.charAt(i))+1);
      }

      int biggest = 0;
      for(Map.Entry<Character,Integer> e : count.entrySet()) {
	       if(e.getValue() > biggest) biggest = e.getValue();
      }

      for(Character c : count.keySet()) {
	       if(count.get(c) == biggest) highest += c;
      } return highest;
   }

   /*
    * checks to see if a given string has balanced brackets, braces and/or
    * parentheses using a stack
    */
   public static boolean isBalanced(String s) {
      if(s.length()==0) return true;
      Stack<Character> stack = new Stack<Character>();
      HashMap<Character, Character> brackets =
	     new HashMap<Character, Character>();

      brackets.put(')','(');
      brackets.put(']','[');
      brackets.put('}','{');
      brackets.put('>','<');

      for(int i=0; i < s.length(); ++i) {
        char c = s.charAt(i);
        if(c=='(' || c=='[' || c=='{' || c=='<') stack.push(c);
        else if(brackets.get(c) != null) {
          if(!stack.empty() && stack.peek() == brackets.get(c))
             stack.pop();
          else return false;
        }
      } if(stack.empty()) return true;
      else return false;
   }

   /*
    * checks if strings are isomorphic
    */
   public static boolean areIsomorphic(String s, String t) {
      if(s.length() != t.length()) return false;
      HashMap<Character, Character> s_t =
	     new HashMap<Character, Character>();
      HashMap<Character, Character> t_s =
	     new HashMap<Character, Character>();

      for(int i=0; i < t.length(); ++i) {
    	 if(s_t.get(s.charAt(i)) == null &&
    	    t_s.get(t.charAt(i)) == null) {
    	     s_t.put(s.charAt(i), t.charAt(i));
    	     t_s.put(t.charAt(i), s.charAt(i));
    	 } else if((s_t.get(s.charAt(i)) != null &&
    		    s_t.get(s.charAt(i)) != t.charAt(i)) ||
    		   (t_s.get(t.charAt(i)) != null &&
    		    t_s.get(t.charAt(i)) != s.charAt(i)))
    	    return false;
      } return true;
   }

   /*
    * returns number of letters to removed to create anagrams
    */
   public static int numberToRemove(String a, String b) {
      if(a.length()==0 || b.length()==0) return b.length() + a.length();

      HashMap<Character, Integer> A = new HashMap<Character, Integer>();
      HashMap<Character, Integer> B = new HashMap<Character, Integer>();
      HashMap<Character, Integer> l = new HashMap<Character, Integer>();

      for(int i=0; i < a.length(); ++i) {
        Character c = a.charAt(i);
        if(l.get(c)==null) l.put(c, i);
        if(A.get(c)!=null) A.put(c, A.get(c)+1);
        else A.put(c, 1);
      } for(int i=0; i < b.length(); ++i) {
        Character c = b.charAt(i);
        if(l.get(c)==null) l.put(c, i);
        if(B.get(c)!=null) B.put(c, B.get(c)+1);
        else B.put(c, 1);
      }

      int total = 0;
      for(Map.Entry<Character, Integer> e : l.entrySet()) {
        Character c = e.getKey();
        if(A.get(c) != null && B.get(c) != null) {
          total+=Math.abs(A.get(c) - B.get(c));
        } else {
          if(A.get(c)!= null) total+=A.get(c);
          if(B.get(c)!= null) total+=B.get(c);
        }
      } return total;
   }

   public static void main(String [] args) {
      // test count_letters
      String a = "aaaabc";
      String b = "abbbbbbc";
      String all = "abcd";
      String name = "ana";
      String empty = "";

      System.out.println("input: " + a);
      String res = countLetters(a);
      System.out.print("output: ");
      for(int i=0; i < res.length(); ++i) System.out.print(res.charAt(i)+" ");
      System.out.println();

      System.out.println("input: "+b);
      res = countLetters(b);
      System.out.print("output: ");
      for(int i=0; i < res.length(); ++i) System.out.print(res.charAt(i)+" ");
      System.out.println();

      System.out.println("input: "+all);
      res = countLetters(all);
      System.out.print("output: ");
      for(int i=0; i < res.length(); ++i) System.out.print(res.charAt(i)+" ");
      System.out.println();

      System.out.println("input: "+empty);
      res = countLetters(empty);
      System.out.print("output: ");
      for(int i=0; i < res.length(); ++i) System.out.print(res.charAt(i)+" ");
      System.out.println();

      System.out.println("input: "+name);
      res = countLetters(name);
      System.out.print("output: ");
      for(int i=0; i < res.length(); ++i) System.out.print(res.charAt(i)+" ");
      System.out.println("\n");

      // test balance stack
      String test = "(<)<<>]]";
      String test2 = "<{[(())]}>";
      String test3 = "[ ] [ ] [ ]";
      String test4 = "(((((";
      String test5 = " )";

      System.out.println("input: "+test);
      if(isBalanced(test)) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: "+test2);
      if(isBalanced(test2)) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: "+test3);
      if(isBalanced(test3)) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: "+test4);
      if(isBalanced(test4)) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: "+test5);
      if(isBalanced(test5)) System.out.println("output: true");
      else System.out.println("output: false");

      // test isomorphism
      System.out.println("input: egg, add");
      if(areIsomorphic("egg", "add")) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: foo, bar");
      if(areIsomorphic("foo", "bar")) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: paper, title");
      if(areIsomorphic("paper", "title")) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: aa, ac");
      if(areIsomorphic("aa", "ac")) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: ac, aa");
      if(areIsomorphic("ac", "aa")) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: ca, ab");
      if(areIsomorphic("ca", "ab")) System.out.println("output: true");
      else System.out.println("output: false");

      System.out.println("input: ab, ca");
      if(areIsomorphic("ab", "ca")) System.out.println("output: true");
      else System.out.println("output: false");
   }
}
